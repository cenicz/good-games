steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE reviews (
            id SERIAL NOT NULL UNIQUE,
            recommended BOOLEAN NOT NULL,
            details TEXT,
            game_id INT NOT NULL REFERENCES gamevo(id) ON DELETE CASCADE,
            user_id INT NOT NULL REFERENCES users(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE users;
        """,
    ]
]
