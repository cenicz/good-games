from pydantic import BaseModel
from psycopg_pool import ConnectionPool
from typing import List
import os

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class DuplicateAccountError(ValueError):
    pass


class DetailsIn(BaseModel):
    game_id: int
    game_name: str
    details: str
    image_href: str
    release_date: str
    platforms: List[str]


class DetailsOut(BaseModel):
    id: int
    game_id: int
    game_name: str
    details: str
    image_href: str
    release_date: str
    platforms: List[str]


class DetailQueries:
    def get_details(self, game_id) -> DetailsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        game_id,
                        game_name,
                        details,
                        release_date,
                        platforms
                        FROM details
                        WHERE game_id = %s;
                        """,
                        [game_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return DetailsOut(
                        id=int(record[0]),
                        game_id=record[1],
                        game_name=record[2],
                        details=record[3],
                        image_href=record[4],
                        release_data=record[5],
                        platforms=record[6],
                    )
        except Exception as e:
            return {"message": "Could not retrieve game details", "error": e}
