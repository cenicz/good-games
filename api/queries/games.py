from pydantic import BaseModel, validator
from psycopg_pool import ConnectionPool
from typing import Union, Optional
import os


pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class DuplicateAccountError(ValueError):
    pass


class Error(BaseModel):
    message: str


class GameIn(BaseModel):
    game_id: int
    game_name: str
    image_href: str


class GameOut(BaseModel):
    id: int
    game_id: int
    game_name: str
    image_href: Optional[str] = None

    @validator("image_href", pre=True, always=True)
    def set_default_image_href(cls, value):
        if value is None:
            default_image_href = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fres.cloudinary.com%2Fteepublic%2Fimage%2Fprivate%2Fs--OXUPFdsw--%2Ft_Preview%2Fb_rgb%3A191919%2Cc_lpad%2Cf_jpg%2Ch_630%2Cq_90%2Cw_1200%2Fv1606803184%2Fproduction%2Fdesigns%2F16724220_0.jpg&f=1&nofb=1&ipt=6fdc38267316a12a8656c6af68dfd1c2c4554db55728f756286a1c9fd155dae8&ipo=images"  # noqa: E501
            return default_image_href
        return value


class GameQueries:
    def get_game(self, game_id) -> Union[Error, GameOut, None]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, game_id, game_name, image_href
                        FROM games
                        WHERE game_id = %s;
                        """,
                        [game_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return GameOut(
                        id=int(record[0]),
                        game_id=record[1],
                        game_name=record[2],
                        image_href=record[3],
                    )
        except Exception as e:
            return {"message": "Could not retrieve games", "error": e}

    def add_game(
        self, game_id: int, image_href: str, game_name: str
    ) -> GameOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO games
                            (
                            game_id,
                            image_href,
                            game_name
                            )
                        VALUES
                        (%s, %s, %s)
                        RETURNING
                        id,
                        game_id,
                        image_href,
                        game_name
                        """,
                        [game_id, image_href, game_name],
                    )
                    record = db.fetchone()
                    game = GameOut(
                        id=record[0],
                        game_id=record[1],
                        image_href=record[2],
                        game_name=record[3],
                    )
                    return game
        except Exception as e:
            return {"message": "Could not add game to database", "error": e}
