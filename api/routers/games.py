from fastapi import APIRouter, HTTPException, Depends, Query
from pydantic import BaseModel
import httpx
from typing import List, Union
from keys import RAWG_API_KEY
from queries.games import GameIn, GameOut, GameQueries

router = APIRouter()


@router.get("/genres", response_model=List[str])
async def fetch_genres():
    return [
        "Action",
        "Indie",
        "Adventure",
        "RPG",
        "Strategy",
        "Shooter",
        "Casual",
        "Simulation",
        "Puzzle",
        "Arcade",
        "Platformer",
        "Massively Multiplayer",
        "Racing",
        "Sports",
        "Fighting",
        "Family",
        "Board Games",
        "Educational",
        "Card",
    ]


@router.get("/tags", response_model=List[str])
async def fetch_tags():
    return [
        "Singleplayer",
        "Multiplayer",
        "Full controller support",
        "Atmospheric",
        "Great Soundtrack",
        "RPG",
        "Co-op",
    ]


@router.get("/orderings", response_model=List[str])
async def fetch_orderings():
    return [
        "name",
        "released",
        "added",
        "created",
        "updated",
        "rating",
        "metacritic",
    ]


class Error(BaseModel):
    message: str


class Game(BaseModel):
    id: int
    game_name: str
    image_href: str


@router.get("/games", response_model=List[Game])
async def get_game_list(page: int = Query(1, ge=1)):
    url = "https://api.rawg.io/api/games"
    games = []
    page_size = 20
    params = {
        "key": RAWG_API_KEY,
        "page": page,
        "page_size": page_size,
    }
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, params=params)
            response.raise_for_status()
            data = response.json()
            results = data.get("results", [])
            games.extend(
                [
                    Game(
                        id=game["id"],
                        game_name=game["name"],
                        image_href=game["background_image"],
                    )
                    for game in results
                ]
            )
    except httpx.HTTPError:
        raise HTTPException(
            status_code=500, detail="Could not retrieve list of games"
        )
    return games


@router.post("/game/add", response_model=GameOut)
def add_game(game: GameIn, query: GameQueries = Depends()):
    try:
        result = query.add_game(game.game_id, game.image_href, game.game_name)
        return result
    except Exception:
        raise HTTPException(
            status_code=500,
            detail="Could not add game to database",
        )


@router.get(
    "/checkgame/{game_id}",
    response_model=Union[Error, GameOut, None],
)
def get_game_db(game_id: int, query: GameQueries = Depends(GameQueries)):
    try:
        result = query.get_game(game_id)
        if result is None:
            return Error(message="game does not exist")
        return result
    except Exception:
        raise HTTPException(
            status_code=500,
            details="Could not retrieve game for specified game id",
        )


@router.get("/search/{search}", response_model=List[GameOut])
async def make_rawg_api_request(
    page: int = 1, page_size: int = 20, search: str = ""
):
    base_url = "https://api.rawg.io/api/games?key="
    url = (
        f"{base_url}{RAWG_API_KEY}"
        f"&page={page}&page_size={page_size}&search={search}"
    )
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        if response.status_code == 200:
            rawg_data = response.json()
            all_games = [
                GameOut(
                    id=game_data["id"],
                    game_name=game_data["name"],
                    image_href=game_data["background_image"],
                    game_id=game_data["id"],
                )
                for game_data in rawg_data.get("results", [])
            ]
            return all_games
        else:
            return []
