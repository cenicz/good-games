import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Reviewed } from "../react-functions/MadeReviews";
import { createReview } from "../react-functions/HandleSubmit";
import GameReviewData from "./ReviewDataPlugin";

function GameDetails() {
  const [canReview, setCanReview] = useState(false);
  const [gameDetail, setGameDetail] = useState([]);
  const [reviews, setReviews] = useState([]);
  const [details, setDetails] = useState("");
  const [recommended, setRecommended] = useState(null);
  const [username, setUsername] = useState("");
  const [userId, setUserId] = useState("");
  const { token } = useToken();
  const { id } = useParams();

  const handleDetailsChange = (event) => {
    setDetails(event.target.value);
  };

  const handleRecommendedChange = (event) => {
    setRecommended(event.target.value === "true");
  };

  async function loadGameDetail() {
    const url = `http://localhost:8000/game/${id}`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setGameDetail(data);
    }
  }

  async function loadReviews() {
    const rUrl = `http://localhost:8000/reviews/game/${id}`;
    const response = await fetch(rUrl);
    if (response.ok) {
      const data = await response.json();
      setReviews(data);
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const reviewData = {
      game_id: Number(id),
      user_id: userId,
      username: username,
      recommended: recommended,
      details: details,
    };
    const gameData = {
      game_id: Number(id),
      image_href: gameDetail.image_href,
      game_name: gameDetail.game_name,
    };
    const gameId = Number(id);
    createReview(reviewData, gameData, token, gameId);
    setCanReview(true);
  };

  useEffect(() => {
    loadGameDetail();
  }, [id]);

  useEffect(() => {
    loadReviews();
  }, [id, token]);

  useEffect(() => {
    const fetchUserInfo = async () => {
      const tokenUrl = `http://localhost:8000/token`;
      const response = await fetch(tokenUrl, { credentials: "include" });
      if (response.ok) {
        const data = await response.json();

        setUserId(data.account.id);
        setUsername(data.account.username);
      }
    };
    fetchUserInfo();
  }, [token]);

  useEffect(() => {
    const fetchReviewStatus = async () => {
      try {
        const canReview = await Reviewed(userId, id);
        setCanReview(!canReview);
      } catch (error) {
        console.error(error);
      }
    };
    fetchReviewStatus();
  }, [userId, id]);

  const paragraphs = gameDetail.details ? gameDetail.details.split("</p>") : [];
  const Englishdetails = paragraphs[0];

  return (
    <>
      <div className="row mt-4">
        <div className="col-md-12">
          <div className="card" key={gameDetail.id}>
            <img
              src={gameDetail.image_href}
              className="card-img-top img short-image"
              alt="Game"
            />
            <div className="card-body">
              <h1 className="card-title">{gameDetail.game_name}</h1>
              <h3 className="mt-3 ">Game Description</h3>
              <span dangerouslySetInnerHTML={{ __html: Englishdetails }} />
            </div>
          </div>
          <GameReviewData />
          <div>
            <div>
              {token ? (
                canReview ? (
                  <p></p>
                ) : (
                  <div className="card mt-4">
                    <div className="card-body">
                      <h3>Review this game?</h3>
                      <form onSubmit={handleSubmit} id="create-review-form">
                        <div className="col">
                          <div className="form-floating mb-3">
                            <input
                              required
                              onChange={handleDetailsChange}
                              value={details}
                              placeholder="Details"
                              type="text"
                              id="details"
                              name="details"
                              className="form-control"
                            />
                            <label htmlFor="details">Details</label>
                          </div>
                          <div className="form-check mb-3">
                            <input
                              required
                              type="radio"
                              id="recommended-yes"
                              name="recommended"
                              value={true}
                              checked={recommended === true}
                              onChange={handleRecommendedChange}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="recommended-yes"
                            >
                              Yes, I recommend this game
                            </label>
                          </div>

                          <div className="form-check mb-3">
                            <input
                              required
                              type="radio"
                              id="recommended-no"
                              name="recommended"
                              value={false}
                              checked={recommended === false}
                              onChange={handleRecommendedChange}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="recommended-false"
                            >
                              No, I do not recommend this game
                            </label>
                          </div>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={canReview}
                          >
                            Submit review
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                )
              ) : (
                <div className="card mt-4">
                  <div className="card-body">
                    <h2 className="card-title">Reviews</h2>
                    <p>
                      Please <a href="/login">login/signup</a> to leave a review
                    </p>
                  </div>
                </div>
              )}
              {reviews.map((review) => (
                <div className="card mt-2">
                  <div key={review.user_id} className="card-body">
                    <h3 className="card-title">User: {review.username}</h3>
                    <h3>{review.game_name}</h3>
                    <p>Details: {review.details}</p>
                    <p>Recommended: {review.recommended ? "Yes" : "No"}</p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default GameDetails;
