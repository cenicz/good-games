import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

function PlatformDetails() {
	const { platformId } = useParams();
	const [platformDetails, setPlatformDetails] = useState(null);

	useEffect(() => {
		async function fetchPlatformDetails() {
			const url = `http://localhost:8000/platform/${platformId}`;
			const response = await fetch(url);
			if (response.ok) {
				const data = await response.json();
				setPlatformDetails(data);
			}
		}

		fetchPlatformDetails();
	}, [platformId]);

	return (
		<div className="container">
			{platformDetails ? (
				<div className="card mt-4">
					<img
						src={platformDetails.image_href}
						className="card-img-top img short-image"
						alt={platformDetails.platform_name}
					/>
					<div className="card-body">
						<h5 className="card-title">{platformDetails.platform_name}</h5>
						{platformDetails.games_count && (
							<p>Number of Games: {platformDetails.games_count}</p>
						)}
						<p
							className="card-text"
							dangerouslySetInnerHTML={{ __html: platformDetails.description }}
						></p>
						{platformDetails.year_start && (
							<p>Started: {platformDetails.year_start}</p>
						)}
						{platformDetails.year_end && (
							<p>Ended: {platformDetails.year_end}</p>
						)}
					</div>
				</div>
			) : (
				<p>Loading...</p>
			)}
		</div>
	);
}

export default PlatformDetails;
