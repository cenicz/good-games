import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import icon from "./icon.png";

function Publishers() {
	const [publishers, setPublishers] = useState([]);

	async function loadPublishers() {
		const url = "http://localhost:8000/publishers";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setPublishers(data);
		}
	}

	useEffect(() => {
		loadPublishers();
	}, []);

	return (
		<div className="container">
			<h1>Publishers</h1>
			<div className="row">
				{publishers.map((publisher) => (
					<div
						key={publisher.id}
						className="col-12 col-sm-6 col-md-4 col-lg-3"
					>
						<div className="card mb-4">
							<img
								src={publisher.image_href ? publisher.image_href : icon}
								className="card-img-top platform-image"
								alt={publisher.publisher_name}
							/>
							<div className="card-body">
								<h5 className="card-title">{publisher.name}</h5>
								<Link
									to={`/publisher/${publisher.id}`}
									className="btn btn-primary"
								>
									View Details
								</Link>
							</div>
						</div>
					</div>
				))}
			</div>
		</div>
	);
}

export default Publishers;
